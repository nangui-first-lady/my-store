create database qu;
use qu;
create table class1(
	classid int not null identity(1,1) primary key,
	classname nvarchar(50) not null
);

-- 创建学生表，存储学生信息，其中字段保护：学生id、姓名、性别、生日、家庭住址，所属班级id
create table student1 (
	studentid int not null identity(1, 1) primary key,
	studentname nvarchar(50),
	studentsex tinyint not null,
	studentbirth date,
	studentaddress nvarchar(255) not null,
	classid int not null ,
	foreign key (classid) references class1(classid)
);
-- 创建课程表，存储课程信息，其中字段包含：课程id、课程名称、课程学分
create table course(
	courseid int identity(1,1) primary key,
	coursename nvarchar(50),
	coursecredit int
);
-- 创建班级课程表，存储班级课程信息，其中字段包含：自增id、班级id、课程id
create table classcourse1(
	classcourseid int identity(1,1) primary key,
	classid int,
	courseid int,
	foreign key(courseid) references course(courseid)
);
select * from course;
insert into course(coursename,coursecredit)
values ('数据库高级应用',3),
('javascript编程基础',3),
('web前端程序设计基础',4),
('动态网页设计.net基础',6);

insert into classcourse1(classid,courseid)
values (1,1),(1,2),(1,3),(1,4);
insert into classcourse1(classid,courseid)
values (2,1),(2,2),(2,3),(2,4);

insert into classcourse1(classid,courseid)
values (3,1),(3,2),(3,3),(3,4);



-- 创建分数表，存储学生每个课程分数信息，其中字段包含：分数id、学生id、课程id、分数
create table score1(
	scoreid int identity(1,1) primary key,
	studentid int,
	courseid int,
	score int,
	foreign key(courseid) references course(courseid)
);
insert into score1(studentid,courseid,score)
values (1,1,80),(1,2,78),(1,3,65),(1,4,90);

insert into score1(studentid,courseid,score)
values (2,1,80),(2,2,77),(2,3,68),(2,4,88);

insert into score1(studentid,courseid,score)
values (3,1,89),(3,2,79),(3,3,89),(3,4,67);

insert into score1(studentid,courseid,score)
values (4,1,65),(4,2,92),(4,3,79),(4,4,97);


insert into class1(classname)
values('软件一班'),('软件二班'),('计算应用技术班');

insert into student1(studentname, studentsex, studentbirth, studentaddress, classid)
values('刘正',1,'2002-08-02','广西省桂林市七星区空明西路10号鸾东小区',1),
	  ('黄贵',1,'2003-07-02','江西省南昌市青山湖区艾溪湖南路南150米广阳小区',1),
	  ('陈美',2,'2002-07-22','福建省龙岩市新罗区曹溪街道万达小区',1);

insert into  student1(studentname, studentsex, studentbirth, studentaddress, classid)
values('江文',1,'2000-08-10','安徽省合肥市庐阳区四里河路与潜山路交汇处万科城市之光',2);

insert into  student1(studentname, studentsex, studentbirth, studentaddress, classid)
values('东方不败',3, '1999-12-11', '河北省平定州西北四十余里的猩猩滩',3);
select*from student1;

select * from student1 where studentbirth like '%2002%';
select * from student1 where studentname like '陈%';
select * from student1 where studentbirth like'20%';
select top 3 *from student1;

select studentid,studentname,studentsex,studentaddress from student1
where studentsex=2;

