

create database eu;
use eu;

create table tblcard(
cardid int primary key identity,
password int,
balance int,
username nvarchar(50)
);

create table tblcomputer(
comid int primary key identity,
onuse tinyint check(onuse=0 or onuse=1),
onte nvarchar(250)
);

create table tblrecord(
reid int primary key identity,
cardid int,
comid int,
begintime datetime,
endtime datetime,
fee nvarchar(250)
foreign key(cardid) references tblcard(cardid),
foreign key(comid) references tblcomputer(comid)
);


