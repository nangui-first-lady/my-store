drop database wu;
create database wu;
use wu;

create table tbl_card(
cardid nvarchar(50) primary key ,
password nvarchar(50) ,
balance int,
username nvarchar(50)
);

create table tbl_computer(
comid nvarchar(50) primary key ,
onuse tinyint check(onuse=0 or onuse=1),
note int
);

create table tbl_record(
reid int primary key ,
cardid nvarchar(50),
comid nvarchar(50),
begintime datetime,
endtime datetime,
fee int,
foreign key(cardid) references tbl_card(cardid),
foreign key(comid) references tbl_computer(comid)
);

insert tbl_card(cardid,password,balance,username) 
values('0023_ABC','555',98,'张军'),
('0025_bbd','abe',300,'朱俊'),
('0036_CCD','何柳',100,'何柳'),
('0045_YGB','0045_YGB',58,'证验'),
('0078_RJV','55885fg',600,'校庆'),
('0089_EDE','zhang',134,'张峻');
select * from tbl_card;

insert tbl_computer(comid,onuse,note)
values('02',0,25555),('03',1,55555),('04',0,66666),
('05',1,88888),('06',0,688878),('B01',0,558558);
select * from tbl_computer;

insert tbl_record(reid,cardid,comid,begintime,endtime,fee)
values(23,'0078_RJV','B01','2007-07-15 19:00:00','2007-07-15 21:00:00',20),
()