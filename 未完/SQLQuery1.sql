use master;

drop database wu;

create database wu;
use wu;
---学生信息表----
create table stuInfo(
stuid int primary key identity not null,
stuname nvarchar(50) not null,
stuage int not null,
stusex tinyint check(stusex=0 or stusex=1) not null,
time datetime 
);
-----课程信息表----
create table courseInfo(
courseid int primary key identity not null,
coursename nvarchar(100) not null,
coursemarks int not null
);
-----分数信息表----
create table scoreInfo(
scoreid int primary key identity not null,
stuid int not null,
courseid int not null,
score int not null,
foreign key(stuid) references stuinfo(stuid),
foreign key(courseid) references courseinfo(courseid)
);
insert stuInfo(stuname,stuage,stusex,time)
values('Tom',19,1,null),
('Jack',20,0,null),
('Rose',21,1,null),
('Lulu',19,1,null),
('LIli',21,0,null),
('abc',20,1,'2007-01-07 01:11:36.590');
select * from stuInfo;

insert courseInfo(coursename,coursemarks)
values('JavaBase',4),
('HTML',2),
('JavaScript',2),
('SqlBase',2);
select * from courseInfo;

insert scoreInfo(stuid,courseid,score)
values(1,1,80),(1,2,85),(1,4,50),
(2,1,75),(2,3,45),(2,4,75),
(3,1,45),
(4,1,95),(4,2,75),(4,3,90),(4,4,45);
select * from scoreInfo;

