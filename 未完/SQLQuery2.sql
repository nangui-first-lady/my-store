create database qi;
use qi;
 create table time2(
 orderid int primary key identity not null,
 orderdate datetime 
 );

 create table item1(
 itemid int primary key identity not null,
 orderid int not null,
 itemtype nvarchar(100) not null,
 itemname nvarchar(100) not null,
 thenumber int not null,
 themoney int not null,
 foreign key(orderid) references time1(orderid)
 );

 insert time2(orderdate) 
 values ('2008-01-12 00:00:00.000'),
 ('2008-02-10 00:00:00.000'),
 ('2008-02-15 00:00:00.000'),
 ('2008-03-10 00:00:00.000');
 select * from time2;

 insert item1(orderid,itemtype,itemname,thenumber,themoney)
 values(1,'文具','笔',72,2),
 (1,'文具','尺',10,1),
 (1,'体育用品','篮球',1,56),
 (2,'文具','笔',36,2),
 (2,'文具','固体胶',20,3),
 (2,'日常用品','透明胶',2,1),
 (2,'体育用品','羽毛球',20,3),
 (3,'文具','订书机',20,3),
 (3,'文具','订书机',20,3),
 (3,'文具','裁纸刀',5,5),
 (4,'文具','笔',20,2),
 (4,'文具','信纸',50,1),
 (4,'日常用品','毛巾',4,5),
 (4,'日常用品','透明胶',30,1),
 (4,'体育用品','羽毛球',20,3);
 select * from time2;
 select * from item1;
 --1
 select itemid 订单编号,orderdate 订单日期,itemtype 类别,itemname 产品名称,thenumber 数量,themoney 单价
 from time2 join item1
 on time2.orderid=item1.orderid;
 --2.查询订购数量大于50的订单的编号，订单日期，订购产品的类别和订购的产品名称
 select itemid 订单编号,orderdate 订单日期,itemtype 类别,itemname 产品名称
 from time2 join item1
 on time2.orderid=item1.orderid
 where thenumber>=50;

 --3.查询所有的订单的订单的编号，订单日期，
 --订购产品的类别和订购的产品名称，订购数量和订购单价以及订购总价


