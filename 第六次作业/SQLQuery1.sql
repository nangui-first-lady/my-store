create database wu;
use wu;
create table class1(
classid int primary key identity,
classname nvarchar(50) not null
);

create table student1(
studentid int primary key identity not null,
studentname nvarchar(50) not null,
studentsex tinyint ,
studentbirth date,
studentaddres nvarchar(250)
);
create table course(
CourseId int identity(1,1),
	CourseName nvarchar(50),
	CourseCredit int
);
insert into course(CourseName) 
values ('数据库高级应用'),('web前端程序设计基础'),('动态网页设计.net基础');

create table ClassCourse(
	ClassCourseId int identity(1,1),
	ClassId int,
	CourseId int
);
insert into ClassCourse(ClassCourseId,ClassId,CourseId) 
values (1,1,1),(2,2,2);
create table Score(
	ScoreId int identity(1,1),
	StudentId int,
	CourseId int,
	Score int
);
insert into Score(Score) values('67'),('76');

insert into class1(classname) values('一班'),('二班'),('三班');
insert into student1(studentname,studentsex,studentbirth,studentaddres)
values ('刘正',1,'2000-01-01','广西省桂林市七星区空明西路10号鸾东小区'),
('黄贵',1,'2001-03-20','江西省南昌市青山湖区艾溪湖南路南150米广阳小区');
select * from class1;
select * from student1;
select * from course;
select * from ClassCourse;
select * from Score;
select studentid from student;
select Score from Score;

select studentname,studentaddres from student1;
select top 1studentname from student1;
